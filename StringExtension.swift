//
//  StringExtension.swift
//  SampleLocal
//
//  Created by zarniaung on 3/1/17.
//  Copyright © 2017 zarniaung. All rights reserved.
//

import Foundation

extension String {
    
    func localized(lang:String) -> String {
        
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
        
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        
    }
    
}
