//
//  ViewController.swift
//  Localization
//
//  Created by zarniaung on 3/3/17.
//  Copyright © 2017 zarniaung. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var language: String?
    @IBOutlet weak var label: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func change(_ sender: Any) {
        let labelText: String = "Label".localized(lang: "en")
        label!.text = labelText
        
    }
    @IBAction func changeFrench(_ sender: Any) {
        let labelText: String = "Label".localized(lang: "fr")
        label!.text = labelText
    }
    
}

